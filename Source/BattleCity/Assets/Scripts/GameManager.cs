﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour
{
    //TODO - увеличить ширину пули (для удаления нескольких клеток) - ?
    //TODO - уменьшить наг танка - ?

    private int _level;
    public int Level { get { return _level; } }

    private static GameManager _instance;
    public static GameManager Instance
    {
        get { return _instance ?? (_instance = FindObjectOfType<GameManager>()); }
        private set { _instance = value; }
    }

    private void Awake()
    {
        Instance = GetComponent<GameManager>();

        _level = PlayerPrefsHelper.GetCurrentLevel();
    }

    private void OnApplicationQuit()
    {
        PlayerPrefsHelper.SetCurrentLevel(_level);
    }

    public void IncreaseLevel()
    {
        _level++;
    }

    public void ResetLevel()
    {
        _level = 0;
    }
}
