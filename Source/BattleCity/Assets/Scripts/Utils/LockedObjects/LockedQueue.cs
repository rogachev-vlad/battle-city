﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utils
{
    public class LockedQueue<T> : Queue<T>
    {
        private object _lock;
        private Queue<T> _objects;

        public LockedQueue()
        {
            _lock = new object();
            _objects = new Queue<T>();
        }

        public new void Enqueue(T t)
        {
            lock (_lock)
            {
                _objects.Enqueue(t);
            }
        }

        public new T Dequeue()
        {
            lock (_lock)
            {
                return _objects.Dequeue();
            }
        }

        public new int Count
        {
            get
            {
                lock (_lock)
                {
                    return _objects.Count;
                }
            }
        }

        public List<T> ToList()
        {
            lock(_lock)
            {
                return _objects.ToList();
            }
        }
    }
}
