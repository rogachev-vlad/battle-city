﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Utils
{
    public class LockedList<T> : List<T>
    {
        private object _lock;
        private List<T> _objects;
        
        public new T this[int key]
        {
            get
            {
                lock (_lock)
                {
                    return _objects[key];
                }
            }
            set
            {
                lock (_lock)
                {
                    _objects[key] = value;
                }
            }
        }

        public LockedList()
        {
            _lock = new object();
            _objects = new List<T>();
        }

        public LockedList(List<T> t)
        {
            _lock = new object();
            _objects = new List<T>(t);
        }

        public new void Add(T t)
        {
            lock (_lock)
            {
                _objects.Add(t);
            }
        }

        public void AddRange(List<T> t)
        {
            lock (_lock)
            {
                _objects.AddRange(t);
            }
        }

        public new int Count
        {
            get
            {
                lock (_lock)
                {
                    return _objects.Count;
                }
            }
        }

        public new void Clear()
        {
            lock (_lock)
            {
                _objects.Clear();
            }
        }
    }
}
