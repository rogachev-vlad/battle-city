﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ControlManager : MonoBehaviour
{
    public static event MoveDelegate OnMove = delegate { };
    public static event TapDelegate OnTap = delegate { };
    public delegate void MoveDelegate(Vector2 direction);
    public delegate void TapDelegate();

    private void Update()
    {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            OnMove(Vector2.up);
        }
        else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            OnMove(Vector2.left);
        }
        else if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))
        {
            OnMove(Vector2.down);
        }
        else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            OnMove(Vector2.right);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            OnTap();
        }
    }

    private void FixedUpdate()
    {
        Vector2 move = new Vector3(CrossPlatformInputManager.GetAxis("Horizontal"),
            CrossPlatformInputManager.GetAxis("Vertical"));
        if (move != Vector2.zero)
        {
            Vector3 v = move - (Mathf.Abs(move.x) < Mathf.Abs(move.y)
                            ? new Vector2(move.x, move.x) : new Vector2(move.y, move.y));
            OnMove(v.normalized);
        }

        if (CrossPlatformInputManager.GetButtonDown("Shoot"))
        {
            OnTap();
        }
    }
}
