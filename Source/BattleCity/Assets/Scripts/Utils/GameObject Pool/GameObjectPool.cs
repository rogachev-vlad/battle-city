﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Utils;

namespace Pool
{
    public class GameObjectPool : MonoBehaviour
    {
        [SerializeField]
        private PooledGameObject _prefab;
        [SerializeField]
        private int _count = 10;

        private LockedQueue<PooledGameObject> _gameObjects;
        private Transform _transform;

        private void Awake()
        {
            _gameObjects = new LockedQueue<PooledGameObject>();
            _transform = transform;

            for (int i = 0; i < _count; i++)
            {
                PooledGameObject obj = Instantiate<PooledGameObject>(_prefab);
                if (obj != null)
                {
                    obj.transform.SetParent(_transform, false);
                    obj.gameObject.SetActive(false);
                    _gameObjects.Enqueue(obj);
                }
            }
        }

        public PooledGameObject GetObject()
        {
            if (_gameObjects.Count == 0)
            {
                PooledGameObject obj = Instantiate<PooledGameObject>(_prefab);
                if (obj != null)
                {
                    obj.transform.SetParent(_transform, false);
                    obj.gameObject.SetActive(false);
                    _gameObjects.Enqueue(obj);
                }
            }
            PooledGameObject pooledGameObject = _gameObjects.Dequeue();
            pooledGameObject.gameObject.SetActive(true);
            return pooledGameObject;
        }

        public List<PooledGameObject> GetObjects(bool isActive = true)
        {
            return _transform.GetComponentsInChildren<PooledGameObject>(isActive).ToList();
        }

        public void ReturnObject(PooledGameObject obj)
        {
            obj.gameObject.SetActive(false);
            _gameObjects.Enqueue(obj);
        }
    }
}
