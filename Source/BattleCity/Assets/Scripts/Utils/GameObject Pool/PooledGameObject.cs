﻿using UnityEngine;
using System.Collections;

namespace Pool
{
    public class PooledGameObject : Objects.InteractiveObject
    {
        private GameObjectPool _pool;
        protected GameObjectPool Pool
        {
            get { return _pool ?? (_pool = GetComponentInParent<GameObjectPool>()); }
        }

        public void ReturnToPool()
        {
            if (Pool)
            {
                Pool.ReturnObject(this);
            }
            else
            {
                Destroy(gameObject);
            }
        }
        
        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("PooledGameObject");
            StartCoroutine(bullet.CheckNextCell());
        }
    }
}
