﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pool;
using UnityEngine.SceneManagement;

public class ObjectHelper : MonoBehaviour
{
    [Tooltip("Parts per cell for an one step")]
    public int Parts = 2;

    [Tooltip("Pool for bullets")]
    public GameObjectPool BulletPool;

    [Tooltip("Cell's parent")]
    public Transform Cells;
    [Tooltip("Border's parent")]
    public Transform Borders;
    [Tooltip("Tank's parent")]
    public Transform Tanks;
    [Tooltip("Tower's parent")]
    public Transform Towers;

    private static Dictionary<string, GameObject> _prefabs;

    public GameObject GetGameObject(string key)
    {
        GameObject prefab;
        if (!_prefabs.TryGetValue(key, out prefab))
        {
            prefab = Resources.Load<GameObject>("Prefabs/" + key);
            _prefabs.Add(key, prefab);
        }
        return prefab;
    }

    private static ObjectHelper _instance;
    public static ObjectHelper Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType<ObjectHelper>();
                _prefabs = new Dictionary<string, GameObject>();
            }
            return _instance;
        }
        private set
        {
            _instance = value;
            _prefabs = new Dictionary<string, GameObject>();
        }
    }

    private void Awake()
    {
        Instance = GetComponent<ObjectHelper>();
    }



    /*private static GameObject _barrierPrefab;
    public static GameObject BarrierPrefab { get { return _barrierPrefab ?? (_barrierPrefab = Resources.Load<GameObject>("Prefabs/Barrier")); } }

    private static Transform _environment;
    public static Transform Environment
    { get { return _environment ?? (_environment = GameObject.Find("Environment").transform); } }

    private static Transform _walls;
    public static Transform barriers
    { get { return _walls ?? (_walls = GameObject.Find("barriers").transform); } }

    private static Transform _tracks;
    public static Transform Tracks
    { get { return _tracks ?? (_tracks = GameObject.Find("Tracks").transform); } }

    private static Texture2D _lightmap;
    public static Texture2D Lightmap
    { get { return _lightmap ?? (_lightmap = Resources.Load<Texture2D>("Tracks")); } }*/

}
