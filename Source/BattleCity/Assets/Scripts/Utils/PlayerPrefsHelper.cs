﻿using UnityEngine;
using System.Collections;

public static class PlayerPrefsHelper
{
    private const string CurrentLevel = "CurrentLevel";

    public static int GetCurrentLevel()
    {
        return PlayerPrefs.GetInt(CurrentLevel);
    }

    public static void SetCurrentLevel(int currentLevel)
    {
        PlayerPrefs.SetInt(CurrentLevel, currentLevel);
    }

    public static void ResetScore()
    {
        PlayerPrefs.DeleteAll();
    }
}
