﻿using UnityEngine;
using System.Collections;
using Objects;

namespace Bonuses
{
    public class Defence : Bonus
    {
        
        public override void Activate(Tank tank)
        {
            base.Activate(tank);
        }

        public override void Deactivate()
        {
            base.Deactivate();
        }
    }

}
