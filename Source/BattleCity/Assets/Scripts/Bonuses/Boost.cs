﻿using UnityEngine;
using System.Collections;
using Components;
using Objects;

namespace Bonuses
{
    public class Boost : Bonus
    {
        private ShootingUnit _shootingUnit;

        public override void Activate(Tank tank)
        {
            _shootingUnit = tank.GetComponent<ShootingUnit>();
            _shootingUnit.SetMaxBulletCount(2 * _shootingUnit.GetMaxBulletCount());

            StartCoroutine(WaitFor());
        }

        public override void Deactivate()
        {
            _shootingUnit.SetMaxBulletCount(_shootingUnit.GetMaxBulletCount() / 2);
        }
    }
}
