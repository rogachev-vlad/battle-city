﻿using UnityEngine;
using System.Collections;
using Objects;

namespace Bonuses
{
    public class Bonus : MonoBehaviour
    {
        [SerializeField]
        protected float Duration;

        public virtual void Activate(Tank tank)
        {

        }
        
        protected IEnumerator WaitFor()
        {
            yield return new WaitForSeconds(Duration);
            Deactivate();
        }

        public virtual void Deactivate()
        {
            
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            other.GetComponent<Tank>().BonusAction(this);
        }
    }

}
