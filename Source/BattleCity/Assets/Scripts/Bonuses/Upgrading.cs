﻿using UnityEngine;
using System.Collections;
using Components;
using Objects;

namespace Bonuses
{
    public class Upgrading : Bonus
    {
        private ShootingUnit _shootingUnit;

        public override void Activate(Tank tank)
        {
            _shootingUnit = tank.GetComponent<ShootingUnit>();
            _shootingUnit.SetArmorPiercing(true);

            StartCoroutine(WaitFor());
        }

        public override void Deactivate()
        {
            _shootingUnit.SetArmorPiercing(false);
        }
    }

}
