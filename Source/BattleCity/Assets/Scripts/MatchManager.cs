﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Components;
using Objects;
using Saving;
using UnityEngine.UI;

public class MatchManager : MonoBehaviour
{
    [SerializeField] private Text _text;

    private int _tankCount;
    private int _towerCount;

    private static MatchManager _instance;
    public static MatchManager Instance
    {
        get { return _instance ?? (_instance = FindObjectOfType<MatchManager>()); }
        private set { _instance = value; }
    }

    private void Awake()
    {
        Instance = this;

        _text.text = "Start";
        _tankCount = 0;
        _towerCount = 0;
    }

    public void StartMatch()
    {
        if (!SavingSystem.Instance.Import(GameManager.Instance.Level.ToString()))
        {
            GameManager.Instance.ResetLevel();
            SavingSystem.Instance.Import(GameManager.Instance.Level.ToString());
        }

        Tank[] tanks = ObjectHelper.Instance.Tanks.GetComponentsInChildren<Tank>();
        _tankCount = tanks.Length;
        _towerCount = ObjectHelper.Instance.Towers.GetComponentsInChildren<Tower>().Length;

        foreach (Tank tank in tanks)
        {
            tank.GetComponent<MovingUnit>().AttachControl();
            tank.GetComponent<ShootingUnit>().AttachControl();
        }
    }

    public void FinishMatch()
    {
        Tank[] tanks = ObjectHelper.Instance.Tanks.GetComponentsInChildren<Tank>();
        foreach (Tank tank in tanks)
        {
            tank.GetComponent<MovingUnit>().DetachControl();
            tank.GetComponent<ShootingUnit>().DetachControl();
        }

        MapGenerator.Instance.Reset();

        _text.text = "Start";
    }

    public void DecreaseTankCount()
    {
        _tankCount--;
        Debug.Log(_tankCount);
        if (_tankCount == 0)
        {
            FinishMatch();

            _text.text = "Defeat!";
            GameManager.Instance.ResetLevel();

            ScreenManager.Instance.OpenScreen(0);
        }
    }

    public void DecreaseTowerCount()
    {
        _towerCount--;
        Debug.Log(_towerCount);
        if (_towerCount == 0)
        {
            FinishMatch();

            _text.text = "Victory!";
            GameManager.Instance.IncreaseLevel();

            ScreenManager.Instance.OpenScreen(0);
        }
    }
}
