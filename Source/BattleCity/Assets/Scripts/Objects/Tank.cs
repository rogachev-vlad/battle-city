﻿using System;
using UnityEngine;
using System.Collections;
using Bonuses;
using Components;

namespace Objects
{
    public class Tank : InteractiveObject
    {
        private HealthUnit _healthUnit;

        public bool IsPlayer;

        protected override void Awake()
        {
            base.Awake();

            _healthUnit = GetComponent<HealthUnit>();
        }
        
        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("Tank");
            StartCoroutine(bullet.CheckNextCell());
            _healthUnit.SetDamage(bullet.Damage);
        }
        
        public void BonusAction(Bonus bonus)
        {
            bonus.Activate(this);
        }

        public override void DecreaseCount()
        {
            MatchManager.Instance.DecreaseTankCount();
        }
    }

}
