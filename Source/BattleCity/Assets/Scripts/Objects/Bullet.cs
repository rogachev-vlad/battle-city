﻿using UnityEngine;
using System.Collections;
using Pool;
using Components;

public class Bullet : PooledGameObject
{
    private Collider2D _collider;
    private Animator _animator;

    private ShootingUnit _shootingUnit;

    [HideInInspector]
    public int Damage;

    private bool _isCount;

    private bool _isArmorPiercing;
    public bool IsArmorPiercing { get { return _isArmorPiercing; } }

    protected override void Awake()
    {
        base.Awake();

        _collider = GetComponent<Collider2D>();
        _animator = GetComponent<Animator>();

        _isCount = false;
        _isArmorPiercing = false;
    }
    
    private void OnEnable()
    {
        _collider.enabled = true;
        _animator.SetBool("IsExplose", false);

        _isCount = false;
        _isArmorPiercing = false;
    }

    public void Launch(ShootingUnit shootingUnit, float velocity, int damage, bool isArmorPiercing = false)
    {
        _shootingUnit = shootingUnit;
        ThisRigidbody.velocity = velocity * ThisTransform.up;
        Damage = damage;
        _isArmorPiercing = isArmorPiercing;

        if (!_isCount)
        {
            _shootingUnit.AddBulletCount(1);
            _isCount = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform != _shootingUnit.transform)
        {
            other.GetComponent<Objects.InteractiveObject>().BulletAction(this);
        }
    }
    
    public IEnumerator CheckNextCell()
    {
        yield return new WaitForFixedUpdate();
        StartDestroying();
    }

    private void StartDestroying()
    {
        if (_isCount)
        {
            _shootingUnit.AddBulletCount(-1);
            _isCount = false;
            _isArmorPiercing = false;
        }

        ThisRigidbody.velocity = Vector2.zero;
        _collider.enabled = false;
        _animator.SetBool("IsExplose", true);
    }

    public override void BulletAction(Bullet bullet)
    {
        //Debug.Log("Bullet");
        StartCoroutine(bullet.CheckNextCell());
        StartDestroying();
    }
}
