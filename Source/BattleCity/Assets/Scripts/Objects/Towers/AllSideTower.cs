﻿using UnityEngine;
using System.Collections;

namespace Objects
{
    public class AllSideTower : Tower
    {

        public override void Attack()
        {
            base.Attack();
        }

        private void FixedUpdate()
        {
            if (Time.fixedTime - StartTime > CheckInterval)
            {
                CheckDirection(ThisTransform.up);
                CheckDirection(-ThisTransform.up);
                CheckDirection(ThisTransform.right);
                CheckDirection(-ThisTransform.right);
                StartTime = Time.fixedTime;
            }
        }

        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("AllSideTower");
            StartCoroutine(bullet.CheckNextCell());
            HealthUnit.SetDamage(bullet.Damage);
        }
    }

}
