﻿using UnityEngine;
using System.Collections;

namespace Objects
{
    public class OneSideTower : Tower
    {
        public override void Attack()
        {
            base.Attack();
        }

        private void FixedUpdate()
        {
            if (Time.fixedTime - StartTime > CheckInterval)
            {
                CheckDirection(ThisTransform.up);
                StartTime = Time.fixedTime;
            }
        }

        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("OneSideTower");
            StartCoroutine(bullet.CheckNextCell());
            HealthUnit.SetDamage(bullet.Damage);
        }
    }

}
