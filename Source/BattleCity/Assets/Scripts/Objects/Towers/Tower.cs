﻿using UnityEngine;
using System.Collections;
using Saving;
using System;
using Components;

namespace Objects
{
    public class Tower : InteractiveObject
    {
        public float CheckIntervalMin = .0f;
        public float CheckIntervalMax = .5f;
        public float Range = 4;

        protected HealthUnit HealthUnit;
        protected ShootingUnit ShootingUnit;

        protected float StartTime;
        protected float CheckInterval;

        protected float SqrRange;

        protected override void Awake()
        {
            base.Awake();

            HealthUnit = GetComponent<HealthUnit>();
            ShootingUnit = GetComponent<ShootingUnit>();

            StartTime = Time.deltaTime;
            CheckInterval = Mathf.Lerp(CheckIntervalMin, CheckIntervalMax, UnityEngine.Random.value);

            SqrRange = Mathf.Pow(Range, 2);
        }

        public virtual void Attack()
        {

        }


        protected void CheckDirection(Vector2 direction)
        {
            RaycastHit2D[] hits = Physics2D.RaycastAll(ThisRigidbody.position, direction);
            if (hits != null && hits.Length > 1)
            {
                if (hits[1].transform.tag == "Tank" &&
                    (hits[1].transform.position - ThisTransform.position).sqrMagnitude < SqrRange)
                {
                    //Debug.DrawRay(ThisRigidbody.position, direction, Color.green);
                    ShootingUnit.Shoot(direction);
                }
            }
        }

        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("Tower");
            StartCoroutine(bullet.CheckNextCell());
            HealthUnit.SetDamage(bullet.Damage);
        }

        public override void DecreaseCount()
        {
            MatchManager.Instance.DecreaseTowerCount();
        }
    }

}
