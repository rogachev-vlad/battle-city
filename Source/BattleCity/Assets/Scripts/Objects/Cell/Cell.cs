﻿using UnityEngine;
using System.Collections;
using Saving;

namespace Objects
{
    public class Cell : InteractiveObject
    {
        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("Cell");
            StartCoroutine(bullet.CheckNextCell());
        }
    }
}
