﻿using UnityEngine;
using System.Collections;

namespace Objects
{
    public class Concrete : Cell
    {
        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("Concrete");
            StartCoroutine(bullet.CheckNextCell());
            if (bullet.IsArmorPiercing)
            {
                Destroy(gameObject);
            }
        }
    }

}
