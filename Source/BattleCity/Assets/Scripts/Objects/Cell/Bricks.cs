﻿using UnityEngine;
using System.Collections;

namespace Objects
{
    public class Bricks : Cell
    {
        public override void BulletAction(Bullet bullet)
        {
            //Debug.Log("Bricks");
            StartCoroutine(bullet.CheckNextCell());
            Destroy(gameObject);
        }
    }

}
