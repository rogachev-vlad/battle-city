﻿using UnityEngine;
using System.Collections;
using Pool;

namespace Components
{
    public class ShootingUnit : Objects.Object
    {
        public float Velocity = 7;
        public int Damage = 60;

        private int _maxBulletCount = 1;
        private int _currentBulletCount;

        private bool _isArmorPiercing;

        protected override void Awake()
        {
            base.Awake();

            _currentBulletCount = 0;
            _isArmorPiercing = false;
        }

        public void AttachControl()
        {
            ControlManager.OnTap += OnTapHandler;
        }

        public void DetachControl()
        {
            ControlManager.OnTap -= OnTapHandler;
        }

        private void OnTapHandler()
        {
            Shoot(ThisTransform.up);
        }

        public void Shoot(Vector2 direction)
        {
            if (_currentBulletCount < _maxBulletCount)
            {
                PooledGameObject bullet = ObjectHelper.Instance.BulletPool.GetObject();
                if (bullet != null)
                {
                    bullet.transform.position = ThisTransform.position;
                    bullet.transform.up = direction;
                    bullet.GetComponent<Bullet>().Launch(this, Velocity, Damage, _isArmorPiercing);
                }
            }
        }

        public void AddBulletCount(int delta)
        {
            _currentBulletCount += delta;
        }

        public int GetMaxBulletCount()
        {
            return _maxBulletCount;
        }

        public void SetMaxBulletCount(int value)
        {
            _maxBulletCount = value;
        }

        public void SetArmorPiercing(bool value)
        {
            _isArmorPiercing = value;
        }
    }

}
