﻿using UnityEngine;
using System.Collections;

namespace Components
{
    public class MovingUnit : Objects.Object
    {
        public float Speed = 2;

        private float _step;
        private float _timePerDistance;
        private bool _isMoving;

        protected override void Awake()
        {
            base.Awake();

            _step = 1f / ObjectHelper.Instance.Parts;
            _timePerDistance = _step / Speed;
            _isMoving = false;
        }

        public void AttachControl()
        {
            ControlManager.OnMove += OnMoveHandler;
        }

        public void DetachControl()
        {
            ControlManager.OnMove -= OnMoveHandler;
        }

        private void OnMoveHandler(Vector2 direction)
        {
            StartCoroutine(MoveToStep(direction));
        }

        public IEnumerator MoveToStep(Vector2 step)
        {
            if (!_isMoving)
            {
                float startTime = Time.fixedTime;
                Vector2 startPosition = RoundPosition(ThisTransform.position);
                Vector2 endPosition = RoundPosition(startPosition + step / ObjectHelper.Instance.Parts);

                /*_rigidbody.rotation = Vector2.Angle(endPosition - startPosition, Vector2.up) *
                    (endPosition.x < startPosition.x ? 1 : -1);*/
                ThisTransform.up = endPosition - startPosition;
                _isMoving = true;

                while (Time.fixedTime - startTime < _timePerDistance)
                {
                    Vector3 nextPosition = Vector2.Lerp(startPosition, endPosition,
                        (Time.fixedTime - startTime) / _timePerDistance);
                    ThisRigidbody.MovePosition(nextPosition);
                    //_rigidbody.rotation = Vector2.Angle(nextPosition, Vector2.right);
                    yield return new WaitForFixedUpdate();
                }
                ThisRigidbody.MovePosition(endPosition);
                //_rigidbody.rotation = Vector2.Angle(endPosition, Vector2.right);
                _isMoving = false;
            }
        }

        private Vector2 RoundPosition(Vector2 position)
        {
            return new Vector2(Mathf.Round(position.x / _step), Mathf.Round(position.y / _step)) * _step;
        }
    }

}
