﻿using UnityEngine;
using System.Collections;

namespace Components
{
    public class HealthUnit : Objects.Object
    {
        [SerializeField]
        private Transform _healthBar;

        public int Health = 125;
        
        private Collider2D _collider;
        private Animator _animator;
        private SpriteRenderer _healthBarSprite;
        private Transform _healthBarTransform;

        private int _currentHealth;

        protected override void Awake()
        {
            base.Awake();

            _collider = GetComponent<Collider2D>();
            _animator = GetComponent<Animator>();
            _healthBarSprite = _healthBar.GetComponentInChildren<SpriteRenderer>();
            _healthBarTransform = _healthBarSprite.transform;

            _currentHealth = Health;
        }

        private void OnEnable()
        {
            _collider.enabled = true;
            _animator.SetBool("IsExplose", false);
        }

        private void Update()
        {
            _healthBar.transform.localRotation = Quaternion.Inverse(ThisTransform.localRotation);
        }

        public void SetDamage(int damage)
        {
            _currentHealth -= damage;
            UpdateHealthBar();

            if (_currentHealth < 0)
            {
                ThisRigidbody.velocity = Vector2.zero;
                _collider.enabled = false;
                _animator.SetBool("IsExplose", true);
            }
        }
        
        public void Destroy()
        {
            Destroy(gameObject);
        }

        private void UpdateHealthBar()
        {
            float t = Mathf.Clamp((float)_currentHealth / Health, 0, 1);
            _healthBarSprite.color = t < 1 ? Color.Lerp(Color.red, Color.green, t) : new Color(0, 0, 0, 0);
            _healthBarTransform.localScale = new Vector3(t, 1, 1);
        }
    }

}