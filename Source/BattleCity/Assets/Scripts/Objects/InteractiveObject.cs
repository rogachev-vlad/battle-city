﻿using UnityEngine;
using System.Collections;

namespace Objects
{
    public class InteractiveObject : Object
    {
        public virtual void BulletAction(Bullet bullet)
        {
            //Debug.Log("InteractiveObject");
            StartCoroutine(bullet.CheckNextCell());
        }

        public virtual void DecreaseCount()
        {

        }
    }
}
