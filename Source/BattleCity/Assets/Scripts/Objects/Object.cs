﻿using UnityEngine;
using System.Collections;

namespace Objects
{
    public class Object : MonoBehaviour
    {
        protected Transform ThisTransform;
        protected Rigidbody2D ThisRigidbody;

        protected virtual void Awake()
        {
            ThisTransform = transform;
            ThisRigidbody = GetComponent<Rigidbody2D>();
        }
    }
}
