﻿// Рогачев Владислав Андреевич ©
// 2015
// rogachev.vlad@gmail.com

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class ScreenManager : MonoBehaviour
{
    private Animator[] _animators;
    public Animator[] Animators { get { return _animators; } }

    private static ScreenManager _instance;
    public static ScreenManager Instance
    {
        get { return _instance ?? (_instance = FindObjectOfType<ScreenManager>()); }
        private set { _instance = value; }
    }


    private void Awake()
    {
        _instance = this;
        _animators = gameObject.GetComponentsInChildren<Animator>(true);
        foreach (Animator animator in _animators)
        {
            animator.gameObject.SetActive(false);
        }
        OpenScreen(0);
    }

    IEnumerator SetDeactive(Animator anim)
    {
        bool closedStateReached = false;
        //bool wantToClose = true;
        while (!closedStateReached)// && wantToClose)
        {
            if (!anim.IsInTransition(0))
            {
                closedStateReached = anim.GetCurrentAnimatorStateInfo(0).IsName("CloseScreenAnimation");
            }
            //wantToClose = !anim.GetBool("OpenScreenAnimation");

            yield return new WaitForEndOfFrame();
        }

        //if (wantToClose)
        //{
            anim.gameObject.SetActive(false);
        //}
    }

    public void OpenScreen(int index)
    {
        for (int i = 0; i < _animators.Length; i++)
        {
            if (_animators[i].gameObject.activeSelf)
            {
                if (_animators[i].GetBool("IsOpen"))
                {
                    _animators[i].SetBool("IsOpen", false);
                    StartCoroutine(SetDeactive(_animators[i]));
                }
            }
        }
        _animators[index].gameObject.SetActive(true);
        _animators[index].SetBool("IsOpen", true);
    }

    public void OpenScreen(GameObject screen)
    {
        for (int i = 0; i < _animators.Length; i++)
        {
            if (_animators[i].gameObject.activeSelf)
            {
                if (_animators[i].GetBool("IsOpen"))
                {
                    _animators[i].SetBool("IsOpen", false);
                    StartCoroutine(SetDeactive(_animators[i]));
                }
            }
        }
        screen.SetActive(true);
        screen.GetComponent<Animator>().SetBool("IsOpen", true);
    }

}
