﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Objects;
using Saving;

public class SavingSystem : MonoBehaviour
{
    private static SavingSystem _instance;

    public static SavingSystem Instance
    {
        get { return _instance ?? (_instance = FindObjectOfType<SavingSystem>()); }
        private set { _instance = value; }
    }

    private void Awake()
    {
        Instance = GetComponent<SavingSystem>();
    }

    public bool Export(string fileName)
    {
        try
        {
            BlockData cells = new BlockData(ObjectHelper.Instance.Cells.gameObject);
            ObjectData[] borders = ObjectHelper.Instance.Borders.GetComponentsInChildren<Collider2D>()
                .Select(x => new ObjectData(x.gameObject)).ToArray();
            TankData[] tanks = ObjectHelper.Instance.Tanks.GetComponentsInChildren<Tank>()
                .Select(x => new TankData(x.gameObject)).ToArray();
            TowerData[] towers = ObjectHelper.Instance.Towers.GetComponentsInChildren<Tower>()
                .Select(x => new TowerData(x.gameObject)).ToArray();

            return XmlSerializator.Instance.Serialize(new Map(cells, borders, tanks, towers), fileName);
        }
        catch (Exception)
        {
            return false;
        }

    }

    public bool Import(string fileName)
    {
        try
        {
            Map map;
            if (XmlSerializator.Instance.Deserialize(fileName, out map))
            {
                MapGenerator.Instance.Build(map);
                return true;
            }
            return false;
        }
        catch (Exception)
        {
            return false;
        }
    }
}
