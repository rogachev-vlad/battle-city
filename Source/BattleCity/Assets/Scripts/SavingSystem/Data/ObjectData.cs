﻿using System.CodeDom;
using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

namespace Saving
{
    [XmlType("ObjectData")]
    [XmlInclude(typeof(CellData))]
    [XmlInclude(typeof(BlockData))]
    [XmlInclude(typeof(TankData))]
    [XmlInclude(typeof(TowerData))]
    public class ObjectData
    {
        [XmlElement("Name")]
        public string Name { get; private set; }

        [XmlElement("Position")]
        public Vector3 Position { get; private set; }

        [XmlElement("Rotation")]
        public Quaternion Rotation { get; private set; }

        [XmlElement("Scale")]
        public Vector3 Scale { get; private set; }
        
        public ObjectData()
        {

        }

        public ObjectData(GameObject thing)
        {
            Name = thing.name;
            Position = thing.transform.position;
            Rotation = thing.transform.rotation;
            Scale = thing.transform.localScale;
        }

        public ObjectData(string name, Vector3 position, Quaternion rotation, Vector3 scale)
        {
            Name = name;
            Position = position;
            Rotation = rotation;
            Scale = scale;
        }

        public GameObject GetGameObject()
        {
            return ObjectHelper.Instance.GetGameObject(Name);
        }
    }
}
