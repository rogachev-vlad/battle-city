﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;
using Objects;

namespace Saving
{
    [XmlType("TowerData")]
    public class TowerData : ObjectData
    {
        [XmlElement("CheckIntervalMin")]
        public float CheckIntervalMin;
        [XmlElement("CheckIntervalMax")]
        public float CheckIntervalMax;
        [XmlElement("Range")]
        public float Range;

        public TowerData()
        {

        }

        public TowerData(GameObject tower) : base(tower)
        {
            Tower thisTower = tower.GetComponent<Tower>();
            CheckIntervalMin = thisTower.CheckIntervalMin;
            CheckIntervalMax = thisTower.CheckIntervalMax;
            Range = thisTower.Range;
        }

        public TowerData(string name, Vector3 position, Quaternion rotation, Vector3 scale, 
            float checkIntervalMin, float checkIntervalMax, int range) : 
            base(name, position, rotation, scale)
        {
            CheckIntervalMin = checkIntervalMin;
            CheckIntervalMax = checkIntervalMax;
            Range = range;
        }
    }
}
