﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Xml.Serialization;
using Components;
using Objects;

namespace Saving
{
    [XmlType("TankData")]
    public class TankData : ObjectData
    {
        [XmlElement("IsPlayer")]
        public bool IsPlayer;
        [XmlElement("Speed")]
        public float Speed;
        [XmlElement("Velocity")]
        public float Velocity;
        [XmlElement("Damage")]
        public int Damage;
        [XmlElement("Health")]
        public int Health;

        public TankData()
        {

        }

        public TankData(GameObject tank) : base(tank)
        {
            IsPlayer = tank.GetComponent<Tank>().IsPlayer;
            Speed = tank.GetComponent<MovingUnit>().Speed;
            ShootingUnit shootingUnit = tank.GetComponent<ShootingUnit>();
            Velocity = shootingUnit.Velocity;
            Damage = shootingUnit.Damage;
            Health = tank.GetComponent<HealthUnit>().Health;
        }

        public TankData(string name, Vector3 position, Quaternion rotation, Vector3 scale, 
            bool isPlayer, float speed, float velocity, int damage, int health) : 
            base(name, position, rotation, scale)
        {
            IsPlayer = isPlayer;
            Speed = speed;
            Velocity = velocity;
            Damage = damage;
            Health = health;
        }
    }
}
