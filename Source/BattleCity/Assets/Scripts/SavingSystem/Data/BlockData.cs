﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Xml.Serialization;

namespace Saving
{
    [XmlType("BlockData")]
    public class BlockData : ObjectData
    {
        [XmlArray("Block")]
        [XmlArrayItem("ObjectData")]
        public ObjectData[] Blocks;

        public BlockData()
        {

        }

        public BlockData(GameObject block) : base(block)
        {
            Blocks = new ObjectData[block.transform.childCount];
            for (int i = 0; i < block.transform.childCount; i++)
            {
                if (block.transform.GetChild(i).tag == "Cell")
                {
                    Blocks[i] = new CellData(block.transform.GetChild(i).gameObject);
                }
                else
                {
                    Blocks[i] = new BlockData(block.transform.GetChild(i).gameObject);
                }
            }
        }

        public BlockData(string name, Vector3 position, Quaternion rotation, Vector3 scale, ObjectData[] blocks) : 
            base(name, position, rotation, scale)
        {
            Blocks = blocks;
        }
    }
}
