﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

namespace Saving
{
    [XmlRoot("Map")]
    [XmlInclude(typeof(ObjectData))]
    [XmlInclude(typeof(CellData))]
    [XmlInclude(typeof(BlockData))]
    [XmlInclude(typeof(TankData))]
    [XmlInclude(typeof(TowerData))]
    public class Map
    {
        [XmlElement("Id")]
        public int Id;

        [XmlElement("Cells")]
        public BlockData Cells;

        [XmlArray("Borders")]
        [XmlArrayItem("ObjectData")]
        public ObjectData[] Borders;

        [XmlArray("Tanks")]
        [XmlArrayItem("TankData")]
        public TankData[] Tanks;

        [XmlArray("Towers")]
        [XmlArrayItem("TowerData")]
        public TowerData[] Towers;

        public Map()
        {

        }

        public Map(BlockData cells, ObjectData[] borders, TankData[] tanks, TowerData[] towers)
        {
            Cells = cells;
            Borders = borders;
            Tanks = tanks;
            Towers = towers;
        }
    }
}
