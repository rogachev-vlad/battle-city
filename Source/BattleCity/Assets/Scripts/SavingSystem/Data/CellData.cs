﻿using UnityEngine;
using System.Collections;
using System.Xml.Serialization;

namespace Saving
{
    [XmlType("CellData")]
    public class CellData : ObjectData
    {
        //[XmlElement("Type")]
        //public string Type;
        
        public CellData()
        {

        }

        public CellData(GameObject cell) : base(cell)
        {
            //Type = cell.GetComponent<SpriteRenderer>().sprite.name;
        }

        public CellData(string name, Vector3 position, Quaternion rotation, Vector3 scale) : 
            base(name, position, rotation, scale)
        {
            //Type = type;
        }
        
    }
}
