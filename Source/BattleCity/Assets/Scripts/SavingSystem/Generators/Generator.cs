﻿using UnityEngine;
using System.Collections;

namespace Saving
{
    public class Generator<T> : MonoBehaviour
    {
        public virtual bool Build(T t) 
        {
            return false;
        }

        public virtual bool Reset()
        {
            return false;
        }
    }

}
