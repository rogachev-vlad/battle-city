﻿using System;
using UnityEngine;
using System.Collections;
using Components;
using Objects;
using Saving;

namespace Saving
{
    public class MapGenerator : Generator<Map>
    {
        private static MapGenerator _instance;

        public static MapGenerator Instance
        {
            get { return _instance ?? (_instance = FindObjectOfType<MapGenerator>()); }
            private set { _instance = value; }
        }

        private void Awake()
        {
            Instance = GetComponent<MapGenerator>();

        }

        public override bool Build(Map map)
        {
            try
            {
                BuildBlocks(map.Cells, ObjectHelper.Instance.Cells);
                BuildBorders(map.Borders, ObjectHelper.Instance.Borders);
                BuildTanks(map.Tanks, ObjectHelper.Instance.Tanks);
                BuildTowers(map.Towers, ObjectHelper.Instance.Towers);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override bool Reset()
        {
            try
            {
                ResetFromParent(ObjectHelper.Instance.Cells);
                ResetFromParent(ObjectHelper.Instance.Borders);
                ResetFromParent(ObjectHelper.Instance.Tanks);
                ResetFromParent(ObjectHelper.Instance.Towers);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void ResetFromParent(Transform parent)
        {
            IEnumerable childs = parent.GetComponentsInChildren<Transform>(true);
            foreach (Transform child in childs)
            {
                if (child != parent)
                {
                    Destroy(child.gameObject);
                }
            }
        }

        private void BuildBlocks(BlockData block, Transform parent)
        {

            for (int i = 0; i < block.Blocks.Length; i++)
            {
                if (block.Blocks[i] is BlockData)
                {
                    GameObject thisGameObject = new GameObject(block.Blocks[i].Name);
                    thisGameObject.transform.position = block.Blocks[i].Position;
                    thisGameObject.transform.rotation = block.Blocks[i].Rotation;
                    thisGameObject.transform.localScale = block.Blocks[i].Scale;
                    thisGameObject.transform.parent = parent;
                    BuildBlocks(block.Blocks[i] as BlockData, thisGameObject.transform);
                }
                else
                {
                    BuildCell(block.Blocks[i] as CellData, parent);
                }
            }
        }

        private void BuildCell(CellData cell, Transform parent)
        {
            GameObject prefab = cell.GetGameObject();
            GameObject thisGameObject = Instantiate(prefab, cell.Position, cell.Rotation) as GameObject;
            if (thisGameObject)
            {
                thisGameObject.name = cell.Name;
                thisGameObject.transform.localScale = cell.Scale;
                thisGameObject.transform.parent = parent;

            }
        }

        private void BuildBorders(ObjectData[] borders, Transform parent)
        {
            for (int i = 0; i < borders.Length; i++)
            {
                GameObject prefab = borders[i].GetGameObject();
                GameObject thisGameObject = Instantiate(prefab, borders[i].Position, borders[i].Rotation) as GameObject;
                if (thisGameObject)
                {
                    thisGameObject.name = borders[i].Name;
                    thisGameObject.transform.localScale = borders[i].Scale;
                    thisGameObject.transform.parent = parent;

                }
            }
        }

        private void BuildTanks(TankData[] tanks, Transform parent)
        {
            for (int i = 0; i < tanks.Length; i++)
            {
                GameObject prefab = tanks[i].GetGameObject();
                GameObject thisGameObject = Instantiate(prefab, tanks[i].Position, tanks[i].Rotation) as GameObject;
                if (thisGameObject)
                {
                    thisGameObject.name = tanks[i].Name;
                    thisGameObject.transform.localScale = tanks[i].Scale;
                    thisGameObject.transform.parent = parent;

                    thisGameObject.GetComponent<Tank>().IsPlayer = tanks[i].IsPlayer;
                    thisGameObject.GetComponent<MovingUnit>().Speed = tanks[i].Speed;
                    ShootingUnit shootingUnit = thisGameObject.GetComponent<ShootingUnit>();
                    shootingUnit.Velocity = tanks[i].Velocity;
                    shootingUnit.Damage = tanks[i].Damage;
                    thisGameObject.GetComponent<HealthUnit>().Health = tanks[i].Health;
                }
            }
        }

        private void BuildTowers(TowerData[] towers, Transform parent)
        {
            for (int i = 0; i < towers.Length; i++)
            {
                GameObject prefab = towers[i].GetGameObject();
                GameObject thisGameObject = Instantiate(prefab, towers[i].Position, towers[i].Rotation) as GameObject;
                if (thisGameObject)
                {
                    thisGameObject.name = towers[i].Name;
                    thisGameObject.transform.localScale = towers[i].Scale;
                    thisGameObject.transform.parent = parent;

                    Tower thisTower = thisGameObject.GetComponent<Tower>();
                    thisTower.CheckIntervalMin = towers[i].CheckIntervalMin;
                    thisTower.CheckIntervalMax = towers[i].CheckIntervalMax;
                    thisTower.Range = towers[i].Range;
                }
            }
        }
    }

}
