﻿using System;
using UnityEngine;
using System.Collections;

namespace Saving
{
    public class Serializator<T>
    {
        public virtual bool Serialize(T t, string fileName)
        {
            return false;
        }

        public virtual bool Deserialize(string fileName, out T t)
        {
            object obj = default(T);
            t = (T)obj;
            return false;
        }
    }

}
