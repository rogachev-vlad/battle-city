﻿using System;
using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace Saving
{
    public class XmlSerializator : Serializator<Map>
    {
        private static XmlSerializator _instance;

        public static XmlSerializator Instance
        {
            get { return _instance ?? (_instance = new XmlSerializator()); }
        }

        private static readonly string _datapath = UnityEngine.Application.dataPath + "/Resources/Maps/";

        private static readonly Type[] _extraTypes =
        {
            typeof(ObjectData), typeof(CellData), typeof(BlockData),
            typeof(TankData), typeof(TowerData)
        };

        public override bool Serialize(Map map, string fileName)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Map), _extraTypes);
                //Directory.CreateDirectory(fileName);
                var encoding = Encoding.GetEncoding("UTF-8");
                StreamWriter fs = new StreamWriter(_datapath + fileName + ".xml", false, encoding);
                //FileStream fs = new FileStream(fileName, FileMode.Create);
                serializer.Serialize(fs, map);
                fs.Close();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public override bool Deserialize(string fileName, out Map map)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Map), _extraTypes);
                var encoding = Encoding.GetEncoding("UTF-8");
                StreamReader fs = new StreamReader(_datapath + fileName + ".xml", encoding);
                //FileStream fs = new FileStream(fileName, FileMode.Open);
                map = (Map)serializer.Deserialize(fs);
                fs.Close();
                return true;
            }
            catch (Exception)
            {
                map = null;
                return false;
            }
        }
    }
}
